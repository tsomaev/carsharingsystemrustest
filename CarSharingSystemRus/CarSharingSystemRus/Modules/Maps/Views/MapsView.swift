//
//  MapsView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit
import GoogleMaps
import SnapKit

protocol MapsViewDelegate: class {
    func didTouchDirectionButton()
    func didTouchScanButton()
    func didTouchReloadButton()
    func didTouchPositionButton()
}

final class MapsView: UIView, CustomizableView {

    // MARK: – Appearance

    enum Appearance {
        static let buttonRightMargin = CGFloat(15)
        static let directionButtonOffset = UIEdgeInsets(
            top: 62,
            left: 0,
            bottom: 0,
            right: Appearance.buttonRightMargin
        )
        static let directionButtonSize = CGSize(width: 50, height: 50)
        static let buttonRadius: CGFloat = 12
        static let scanButtonOffset = UIEdgeInsets(
            top: 116,
            left: 0,
            bottom: 0,
            right: Appearance.buttonRightMargin
        )
        static let scanButtonSize = CGSize(width: 50, height: 50)
        static let sectionSize = CGSize(width: 50, height: 105)
        static let sectionWithTwoButtonsOffset = UIEdgeInsets(
            top: 18,
            left: 0,
            bottom: 0,
            right: Appearance.buttonRightMargin
        )
    }

    // MARK: – Private Properties

    private lazy var scanButton: ScanButton = {
        let frame = CGRect(origin: .zero, size: Appearance.scanButtonSize)
        return ScanButton(frame: frame)
    }()

    private lazy var directionButton: DirectionButton = {
        let frame = CGRect(origin: .zero, size: Appearance.directionButtonSize)
        return DirectionButton(frame: frame)
    }()

    private lazy var sectionWithTwoButtons: SectionWithTwoButtons = {
        let frame = CGRect(origin: .zero, size: Appearance.sectionSize)
        return SectionWithTwoButtons(frame: frame)
    }()

    private lazy var maps = GMSMapView(frame: .zero)
    private weak var delegate: MapsViewDelegate?
    private var presentedMarkersOnMap: [GMSMarker] = []

    // MARK: – Public properties

    final var isMyLocationEnabled: Bool {
        set(isEnabled) {
            self.maps.isMyLocationEnabled = isEnabled
        }
        get {
            return self.maps.isMyLocationEnabled
        }
    }

    final var currentZoom: Float {
        return self.maps.camera.zoom
    }

    var visibleMarkers: [GMSMarker] {
        return self.presentedMarkersOnMap.filter {
            self.maps.projection.contains($0.position)
        }
    }

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func set(_ newCamera: GMSCameraPosition) {
        self.maps.camera = newCamera
    }

    final func animate(toCamera newCamera: GMSCameraPosition, completion: @escaping (() -> Void)) {
        self.maps.animate(to: newCamera)
        completion()
    }

    final func set(_ delegate: MapsViewDelegate) {
        self.delegate = delegate
        self.directionButton.set(delegate)
        self.scanButton.set(delegate)
        self.sectionWithTwoButtons.set(delegate)
    }

    final func setupMapDelegate(_ delegate: GMSMapViewDelegate) {
        self.maps.delegate = delegate
    }

    final func set(_ locationDirection: CLLocationDirection) {
        self.maps.animate(toBearing: locationDirection)
    }

    final func set(_ markers: [GMSMarker]) {
        self.maps.clear()
        self.presentedMarkersOnMap.removeAll()
        markers.forEach { $0.map = self.maps }
        self.presentedMarkersOnMap = markers
    }

    // MARK: – Private Methods

    private func setupBackground() {
        self.backgroundColor = UIColor.Interface.defaultBackground
    }

    private func setupDirectionButton() {
        self.directionButton.backgroundColor = UIColor.Interface.defaultBackground
        self.directionButton.layer.cornerRadius = Appearance.buttonRadius
        self.directionButton.clipsToBounds = true
    }

    private func setupScanButton() {
        self.scanButton.backgroundColor = UIColor.Interface.main
        self.scanButton.layer.masksToBounds = true
        self.scanButton.layer.cornerRadius = Appearance.buttonRadius
        self.scanButton.setImage(
            UIImage(named: String.ImageNames.ScanOnMap),
            for: .normal
        )
    }

    private func makeConstraintsForMap() {
        self.maps.translatesAutoresizingMaskIntoConstraints = false
        self.maps.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func makeConstraintsForDirectionButton() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.directionButton.snp.makeConstraints { make in
            make.size.equalTo(Appearance.directionButtonSize)
            make.top.equalTo(Appearance.directionButtonOffset.top)
            make.right.equalToSuperview().offset(-Appearance.directionButtonOffset.right)
        }
    }

    private func makeConstraintsForScanButton() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.scanButton.snp.makeConstraints { make in
            make.size.equalTo(Appearance.scanButtonSize)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-Appearance.scanButtonOffset.right)
        }
    }

    private func makeConstraintsForSectionWithTwoButtons() {
        defer {
            self.sectionWithTwoButtons.layoutSubviews()
        }
        self.sectionWithTwoButtons.snp.makeConstraints { make in
            make.top.equalTo(self.scanButton.snp.bottom)
                .offset(Appearance.sectionWithTwoButtonsOffset.top)
            make.right.equalToSuperview()
                .offset(-Appearance.sectionWithTwoButtonsOffset.right)
            make.width.equalTo(Appearance.sectionSize.width)
        }
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.maps)
        self.addSubview(self.directionButton)
        self.addSubview(self.scanButton)
        self.addSubview(self.sectionWithTwoButtons)
    }

    func setupSubviews() {
        self.setupBackground()
    }

    func makeConstraints() {
        self.makeConstraintsForMap()
        self.makeConstraintsForDirectionButton()
        self.makeConstraintsForScanButton()
        self.makeConstraintsForSectionWithTwoButtons()
    }

}
