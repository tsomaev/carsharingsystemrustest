//
//  RentView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit
import GoogleMaps

protocol RentViewDelegate: class {
    func didTouchLeftButtonInFooter()
    func didTouchRightButtonInFooter()
}

class RentView: UIView, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        // Information:
        static let informationViewBackgroundColor: UIColor = .red
        // Footer:
        static let footerHeight: CGFloat = 47
        // Maps:
        static let mapsMarginBottomOffset: CGFloat = 70
    }

    // MARK: – Data

    typealias InformationViewModel = RentViewModel.DataForInformationView
    private let dataForInformationView: InformationViewModel

    // MARK: – Public Properties

    final var isMyLocationEnabled: Bool {
        set(isEnabled) {
            self.maps.isMyLocationEnabled = isEnabled
        }
        get {
            return self.maps.isMyLocationEnabled
        }
    }

    // MARK: – Private Properties

    private weak var delegate: RentViewDelegate?

    // MARK: – UI

    private lazy var informationView = RentInformationView(
        withData: self.dataForInformationView
    )

    private lazy var footer = RentFooterView()
    private lazy var maps = GMSMapView(frame: .zero)

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    init(frame: CGRect, withDataAboutDevice data: InformationViewModel) {
        self.dataForInformationView = data
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func set(_ delegate: RentViewDelegate) {
        self.footer.set(delegate)
    }

    final func setMapDelegate(_ delegate: GMSMapViewDelegate) {
        self.maps.delegate = delegate
    }

    final func set(_ newCamera: GMSCameraPosition) {
        self.maps.camera = newCamera
    }

    final func animate(toCamera newCamera: GMSCameraPosition, completion: @escaping (() -> Void)) {
        self.maps.animate(to: newCamera)
        completion()
    }

    // MARK: – Private Methods

    private func setupInformationView() {
        self.informationView.backgroundColor = Appearance.informationViewBackgroundColor
    }

    private func setupFooter() {
        self.footer.isUserInteractionEnabled = true
    }

    private func makeConstraintsForInformationView() {
        self.informationView.translatesAutoresizingMaskIntoConstraints = false
        self.informationView.snp.makeConstraints { make in
            make.bottom.equalTo(self.footer.snp.top)
            make.left.right.equalToSuperview()
        }
    }

    private func makeConstraintsForFooter() {
        self.footer.translatesAutoresizingMaskIntoConstraints = false
        self.footer.snp.makeConstraints { make in
            make.bottom.left.right.equalToSuperview()
            make.height.equalTo(Appearance.footerHeight)
        }
    }

    private func makeConstraintsForMap() {
        self.maps.translatesAutoresizingMaskIntoConstraints = false
        self.maps.snp.makeConstraints { make in
            if #available(iOS 12.0, *) {
                make.top.equalTo(safeAreaLayoutGuide.snp.top)
            } else {
                make.top.equalToSuperview()
            }
            make.left.right.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.64)
        }
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.maps)
        self.addSubview(self.informationView)
        self.addSubview(self.footer)
    }

    func setupSubviews() {
        self.setupInformationView()
        self.setupFooter()
    }

    func makeConstraints() {
        self.makeConstraintsForFooter()
        self.makeConstraintsForInformationView()
        self.makeConstraintsForMap()
    }

}
