//
//  RentInformationView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class RentInformationView: UIView, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        // Fonts:
        static let largeFont = UIFont.custom(ofSize: 18, withWeight: .bold)!
        static let mediumFont = UIFont.custom(ofSize: 12, withWeight: .bold)!
        static let regularFont = UIFont.custom(ofSize: 12, withWeight: .regular)!
        // Bicycle:
        static let bicycleImage = UIImage(named: String.ImageNames.Bicycle)
        static let bicycleSize = CGSize(width: 158, height: 111)
        static let bicycleContentMode = UIView.ContentMode.scaleAspectFit
        static let bicycleClipToBounds = true
        static let bicycleBackgroundColor = UIColor.clear
        // Stack View:
        static let stackViewBottomMargin: CGFloat = 14
        static let stackViewAlignment = UIStackView.Alignment.center
        static let stackViewAxis = NSLayoutConstraint.Axis.vertical
        static let stackViewSpacing: CGFloat = 8
        // Labels:
        static let labelTextAlignment = NSTextAlignment.center
        static let labelTextColor = UIColor.Interface.defaultText
        static let numberOfLinesInLabel = 1
        static let titlePriceLabelLineSpacing: CGFloat = 22
        static let subtitlePriceLabelLineSpacing: CGFloat = 22
        static let identifierLabelLineSpacing: CGFloat = 32
        static let nameLabelLineSpacing: CGFloat = 15
        static let batteryLabelLineSpacing: CGFloat = 15
    }

    // MARK: – Attributed Key

    typealias AttributedKey = NSAttributedString.Key

    // MARK: – Font Type

    private enum FontType {
        case large
        case medium
        case regular
    }

    // MARK: – Data

    typealias ViewModel = RentView.InformationViewModel
    let data: ViewModel

    // MARK: – UI

    private lazy var contentStackView = UIStackView()
    private lazy var bicycleView = UIImageView()
    private lazy var titlePriceLabel = UILabel()
    private lazy var subtitlePriceLabel = UILabel()
    private lazy var identifierLabel = UILabel()
    private lazy var nameLabel = UILabel()
    private lazy var batteryLabel = UILabel()

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    init(frame: CGRect = .zero, withData data: ViewModel) {
        self.data = data
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Private Methods

    private func setupContentStackView() {
        self.contentStackView.axis = Appearance.stackViewAxis
        self.contentStackView.alignment = Appearance.stackViewAlignment
        self.contentStackView.spacing = Appearance.stackViewSpacing
    }

    private func setupBicycleView() {
        self.bicycleView.contentMode = Appearance.bicycleContentMode
        self.bicycleView.image = Appearance.bicycleImage
        self.bicycleView.clipsToBounds = Appearance.bicycleClipToBounds
        self.bicycleView.backgroundColor = Appearance.bicycleBackgroundColor
    }

    private func setupTitlePriceLabel() {
        self.titlePriceLabel.numberOfLines = Appearance.numberOfLinesInLabel
        self.titlePriceLabel.attributedText = self.generateAttributedString(
            string: self.data.pricePerMinuteText,
            font: Appearance.largeFont,
            lineSpacing: Appearance.titlePriceLabelLineSpacing,
            textColor: Appearance.labelTextColor
        )
    }

    private func setupSubtitlePriceLabel() {
        self.subtitlePriceLabel.numberOfLines = Appearance.numberOfLinesInLabel
        self.subtitlePriceLabel.attributedText = self.generateAttributedString(
            string: self.data.priceToUnlockText,
            font: Appearance.largeFont,
            lineSpacing: Appearance.subtitlePriceLabelLineSpacing,
            textColor: Appearance.labelTextColor
        )
    }

    private func setupIdentifierLabel() {
        self.identifierLabel.numberOfLines = Appearance.numberOfLinesInLabel
        self.identifierLabel.attributedText = self.generateAttributedString(
            string: self.data.deviceNumberText,
            font: Appearance.mediumFont,
            lineSpacing: Appearance.identifierLabelLineSpacing,
            textColor: Appearance.labelTextColor
        )
    }

    private func setupNameLabel() {
        self.identifierLabel.numberOfLines = Appearance.numberOfLinesInLabel
        self.identifierLabel.attributedText = self.generateAttributedString(
            string: self.data.deviceNameText,
            font: Appearance.regularFont,
            lineSpacing: Appearance.nameLabelLineSpacing,
            textColor: Appearance.labelTextColor
        )
    }

    private func setupBatteryLabel() {
        self.batteryLabel.numberOfLines = Appearance.numberOfLinesInLabel
        self.batteryLabel.attributedText = self.generateAttributedString(
            string: self.data.batteryLevelText,
            font: Appearance.regularFont,
            lineSpacing: Appearance.nameLabelLineSpacing,
            textColor: Appearance.labelTextColor
        )
    }

    private func makeConstraintsForContentStackView() {
        self.contentStackView.translatesAutoresizingMaskIntoConstraints = false
        self.contentStackView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.left.greaterThanOrEqualToSuperview()
            make.right.lessThanOrEqualToSuperview()
            make.bottom.equalToSuperview().offset(-Appearance.stackViewBottomMargin)
        }
    }

    private func makeConstraintsForBicycleView() {
        self.bicycleView.translatesAutoresizingMaskIntoConstraints = false
        self.bicycleView.snp.makeConstraints { make in
            make.size.equalTo(Appearance.bicycleSize)
        }
    }

    // MARK: – Helpers

    private func generateAttributedString(
        string: String,
        font: UIFont,
        lineSpacing: CGFloat,
        textColor: UIColor
    ) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = Appearance.labelTextAlignment
        paragraphStyle.lineSpacing = lineSpacing
        return NSAttributedString(
            string: string,
            attributes: [
                AttributedKey.font: font,
                AttributedKey.paragraphStyle: paragraphStyle,
                AttributedKey.foregroundColor: textColor
            ]
        )
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.contentStackView)
        self.contentStackView.addArrangedSubview(self.bicycleView)
        self.contentStackView.addArrangedSubview(self.titlePriceLabel)
        self.contentStackView.addArrangedSubview(self.subtitlePriceLabel)
        self.contentStackView.addArrangedSubview(self.identifierLabel)
        self.contentStackView.addArrangedSubview(self.nameLabel)
        self.contentStackView.addArrangedSubview(self.batteryLabel)
    }

    func setupSubviews() {
        self.setupContentStackView()
        self.setupBatteryLabel()
        self.setupNameLabel()
        self.setupIdentifierLabel()
        self.setupSubtitlePriceLabel()
        self.setupTitlePriceLabel()
        self.setupBicycleView()
    }

    func makeConstraints() {
        self.makeConstraintsForContentStackView()
        self.makeConstraintsForBicycleView()
    }

}
