// 
//  RentViewModel.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation

final class RentViewModel {

	// MARK: - Properties

	weak private var viewController: ViewControllerWithCallbacks!
    private var router: RentRouter!

    private let defaultTextAboutPricePerMinute = "Бесплатно!"
    private let defaultTextAboutPriceToUnlock = "И разблокировка бесплатная!"
    private let defaultTextAboutBatteryLevel = "Полностью разряжен!"
    private let defaultTextAboutDeviceIdentifier = "Отсутствует порядковый номер"

    // MARK: – Information View Model

    struct DataForInformationView {
        let pricePerMinuteText: String
        let priceToUnlockText: String
        let deviceNumberText: String
        let deviceNameText: String
        let batteryLevelText: String

        init(
            aboutMinute minute: String,
            aboutUnlocking unlock: String,
            aboutNumber number: String,
            aboutName name: String,
            aboutBattery level: String
        ) {
            self.pricePerMinuteText = minute
            self.priceToUnlockText = unlock
            self.deviceNumberText = number
            self.deviceNameText = name
            self.batteryLevelText = level
        }
    }

	// MARK: - Object Life Cycle

    init() {
        self.additionInitialization()
    }

	convenience init(withViewController viewController: ViewControllerWithCallbacks) {
		self.init()
		self.viewController = viewController
	}

    private func additionInitialization() {
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
        self.router = RentRouter(withViewModel: self)
    }

	deinit {
        #if DEBUG
		print("\n[\(self) deinit]\n")
        #endif
	}

    // MARK: – Getters

    final var dataForInformationView: DataForInformationView {
        return DataForInformationView(
            aboutMinute: self.textPriceAboutMinute,
            aboutUnlocking: self.textPriceAboutUnlocking,
            aboutNumber: self.deviceIdentifier,
            aboutName: self.deviceName,
            aboutBattery: self.textAboutBatteryLevel
        )
    }

    final var navigationTitleText: String {
        let titleFromModel = self.router.title
        return titleFromModel.isEmpty ? "" : "ул. \(titleFromModel)"
    }

    private var textPriceAboutMinute: String {
        let price = self.router.pricePerMinute
        let free = price == 0.0
        let text = "\(price)₽ минута"
        return free ? self.defaultTextAboutPricePerMinute : text
    }

    private var textPriceAboutUnlocking: String {
        let price = self.router.priceToUnlock
        let free = price == 0.0
        let text = "\(price)₽ разблокировка"
        return free ? self.defaultTextAboutPriceToUnlock : text
    }

    private var deviceIdentifier: String {
        let identifier = self.router.deviceIdentifier
        let noID = identifier == 0
        let text = "№ \(identifier)"
        return noID ? self.defaultTextAboutDeviceIdentifier : text
    }

    private var deviceName: String {
        return self.router.deviceName
    }

    private var textAboutBatteryLevel: String {
        let level = self.router.batteryLevel
        let discharged = level == 0
        let text = "Заряд: \(level)%"
        return discharged ? self.defaultTextAboutBatteryLevel : text
    }

    // MARK: – Public Accessors

    final func saveScooter(_ scooter: Scooter) {
        self.router.saveData(scooter)
    }

    final func handleFooterLeftButton() {
        self.router.initiateRent()
    }

    final func handleFooterRightButton() {
        self.router.initiateBooking()
    }

}

// MARK: – Callbacks

extension RentViewModel: ViewModelWithCallbacks {

    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.viewController.didGetBadErrorWithTitle(title, andMessage: rawMessage)
    }

    func didGetNewData() {
        self.viewController.didGetNewData()
    }

    func didGetEmptyData() {
        self.viewController.didGetEmptyData()
    }

}
