//
//  ScooterTableViewCell.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class ScooterTableViewCell: UITableViewCell, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        static let backgroundColor = UIColor.Interface.defaultBackground
        static let shadowColor = UIColor.Interface.defaultText.cgColor
        static let shadowOpacity: Float = 0.15
        static let shadowOffset = CGSize(width: 4, height: 20)
        static let shadowRadius: CGFloat = 20
        static let shadowScale = true
        static let contentViewOffsets = UIEdgeInsets(
            top: 16,
            left: 16,
            bottom: 0,
            right: -16
        )
        static let contentHeight: CGFloat = 64
    }

    // MARK: – View Data

    private var data: PlaceView.CellViewModel

    // MARK: – UI

    private lazy var customContentView: ScooterCellContentView = {
        let totalWidth = UIScreen.main.bounds.size.width
        let horizontalTotalMargin = Appearance.contentViewOffsets.left * 2
        let width = totalWidth - horizontalTotalMargin
        let size = CGSize(width: width, height: Appearance.contentHeight)
        let frame = CGRect(origin: .zero, size: size)
        let view = ScooterCellContentView(frame: frame, withData: self.data)
        return view
    }()

    // MARK: – Initialization

    init(
        style: UITableViewCell.CellStyle,
        reuseIdentifier: String?,
        withData data: PlaceView.CellViewModel
    ) {
        self.data = data
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    // MARK: – Public Methods

    final func clearSpareViews() {
        self.contentView.subviews.forEach { subview in
            if subview == self.customContentView {
                subview.clearShadow()
            }
            subview.removeFromSuperview()
        }
    }

    // MARK: – Private Methods

    private func setupCustomContentView() {
        self.customContentView.backgroundColor = Appearance.backgroundColor
        self.customContentView.applyShadow(
            withColor: Appearance.shadowColor,
            opacity: Appearance.shadowOpacity,
            offset: Appearance.shadowOffset,
            radius: Appearance.shadowRadius,
            scale: Appearance.shadowScale
        )
    }

    private func makeConstraintsForCustomContentView() {
        self.customContentView.translatesAutoresizingMaskIntoConstraints = false
        self.customContentView.snp.makeConstraints { make in
            make.left
                .equalToSuperview()
                .offset(Appearance.contentViewOffsets.left)
            make.right
                .equalToSuperview()
                .offset(Appearance.contentViewOffsets.right)
            make.width
                .equalToSuperview()
                .offset(Appearance.contentViewOffsets.right * 2) // -32 points
            make.top
                .equalToSuperview()
                .offset(Appearance.contentViewOffsets.top)
            make.bottom.lessThanOrEqualTo(self.contentView.snp.bottom)
            make.height.equalTo(Appearance.contentHeight)
        }
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.contentView.addSubview(self.customContentView)
    }

    func setupSubviews() {
        self.selectionStyle = .none
        self.setupCustomContentView()
    }

    func makeConstraints() {
        self.makeConstraintsForCustomContentView()
    }

}
