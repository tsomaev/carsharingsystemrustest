//
//  ScooterCellContentView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class ScooterCellContentView: UIView, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        static let scooterViewSize = CGSize(width: 42.5, height: 36)
        static let scooterLeftMargin: CGFloat = 20
        static let scooterContentMode = UIView.ContentMode.scaleAspectFit
        static let scooterIcon = UIImage(named: String.ImageNames.ScooterInsideCell)!
        static let imageViewClipsToBounds = true
        static let accessoryWidth: CGFloat = 37
        static let accessoryBackgroundColor = UIColor.Interface.main
    }

    // MARK: – Data

    private let data: PlaceView.CellViewModel

    // MARK: – UI

    private lazy var scooterView = UIImageView()

    private lazy var centerView: ScooterCellCenterView = {
        let view = ScooterCellCenterView(
            withData: self.data
        )
        return view
    }()

    private lazy var accessoryView = ScooterCellAccessoryView()

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    init(frame: CGRect = .zero, withData data: PlaceView.CellViewModel) {
        self.data = data
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func clearSubviews() {
        self.scooterView.removeFromSuperview()
        self.centerView.removeFromSuperview()
        self.accessoryView.removeFromSuperview()
    }

    // MARK: – Private Methods

    private func setupScooterView() {
        self.scooterView.image = Appearance.scooterIcon
        self.scooterView.contentMode = Appearance.scooterContentMode
        self.scooterView.frame.size = Appearance.scooterViewSize
        self.scooterView.clipsToBounds = Appearance.imageViewClipsToBounds
    }

    private func setupCenterView() {
        self.centerView.backgroundColor = UIColor.Interface.defaultBackground
    }

    private func setupAccessoryView() {
        self.accessoryView.backgroundColor = Appearance.accessoryBackgroundColor
    }

    private func makeConstraintsForScooterView() {
        self.scooterView.translatesAutoresizingMaskIntoConstraints = false
        self.scooterView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(Appearance.scooterLeftMargin)
            make.size.equalTo(Appearance.scooterViewSize)
        }
    }

    private func makeConstraintsForCenterView() {
        self.centerView.translatesAutoresizingMaskIntoConstraints = false
        self.centerView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.right.equalTo(self.accessoryView.snp.left)
            make.left.equalTo(self.scooterView.snp.right)
        }
    }

    private func makeConstraintsForAccessoryView() {
        self.accessoryView.translatesAutoresizingMaskIntoConstraints = false
        self.accessoryView.snp.makeConstraints { make in
            make.width.equalTo(Appearance.accessoryWidth)
            make.right.bottom.top.equalToSuperview()
        }
        self.accessoryView.layoutSubviews()
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.scooterView)
        self.addSubview(self.centerView)
        self.addSubview(self.accessoryView)
    }

    func setupSubviews() {
        self.setupScooterView()
        self.setupCenterView()
        self.setupAccessoryView()
    }

    func makeConstraints() {
        self.makeConstraintsForScooterView()
        self.makeConstraintsForCenterView()
        self.makeConstraintsForAccessoryView()
    }

}
