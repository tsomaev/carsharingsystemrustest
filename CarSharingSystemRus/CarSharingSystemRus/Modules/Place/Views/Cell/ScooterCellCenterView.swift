//
//  ScooterCellCenterView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class ScooterCellCenterView: UIView, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        static let horizontalMargin: CGFloat = 25
        static let labelFont = UIFont.custom(ofSize: 14, withWeight: .regular)!
        static let textColor = UIColor.Interface.defaultText
        static let textAlignment = NSTextAlignment.left
        static let numberOfLinesInLabel = 1
        static let stackViewSpacing: CGFloat = .zero
        static let stackViewAlignment = UIStackView.Alignment.leading
        static let stackViewAxis = NSLayoutConstraint.Axis.vertical
    }

    // MARK: – Attributed Key

    typealias AttributedKey = NSAttributedString.Key

    // MARK: – Data

    private let data: PlaceView.CellViewModel

    // MARK: – UI

    private lazy var titleLabel = UILabel()
    private lazy var subtitleLabel = UILabel()
    private lazy var labelStackView = UIStackView()

    // MARK: – Initialization

    init(frame: CGRect = .zero, withData data: PlaceView.CellViewModel) {
        self.data = data
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    // MARK: – Private Methods

    private func setupLabelStackView() {
        self.labelStackView.alignment = Appearance.stackViewAlignment
        self.labelStackView.axis = Appearance.stackViewAxis
        self.labelStackView.spacing = Appearance.stackViewSpacing
    }

    private func setupTitleLabel() {
        self.titleLabel.attributedText = self.generateAttributedString(
            byString: self.data.title
        )
        self.titleLabel.numberOfLines = Appearance.numberOfLinesInLabel
    }

    private func setupSubtitleLabel() {
        self.subtitleLabel.attributedText = self.generateAttributedString(
            byString: self.data.subtitle
        )
        self.subtitleLabel.numberOfLines = Appearance.numberOfLinesInLabel
    }

    private func makeConstraintsForLabelStackView() {
        self.labelStackView.translatesAutoresizingMaskIntoConstraints = false
        self.labelStackView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(Appearance.horizontalMargin)
            make.right.equalToSuperview().offset(-Appearance.horizontalMargin)
        }
    }

    private func generateAttributedString(byString string: String) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = Appearance.textAlignment
        return NSAttributedString(
            string: string,
            attributes: [
                AttributedKey.font: Appearance.labelFont,
                AttributedKey.paragraphStyle: paragraphStyle
            ]
        )
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.labelStackView.addArrangedSubview(self.titleLabel)
        self.labelStackView.addArrangedSubview(self.subtitleLabel)
        self.addSubview(self.labelStackView)
    }

    func setupSubviews() {
        self.setupLabelStackView()
        self.setupTitleLabel()
        self.setupSubtitleLabel()
    }

    func makeConstraints() {
        self.makeConstraintsForLabelStackView()
    }
}
