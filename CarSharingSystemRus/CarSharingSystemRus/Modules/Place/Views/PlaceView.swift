//
//  PlaceView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

protocol PlaceViewDelegate: class {
    func needsToUpdateTableSections()
    func needsToUpdateTableRows()
    func needsToRenderCellAtRow(_ row: Int)
    func needsToOpenScooterAtRow(_ row: Int)
}

class PlaceView: UIView, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        // General:
        static let backgroundColor = UIColor.Interface.defaultBackground
        // Table and cells:
        static let cellHeight: CGFloat = 96
        static let tableSeparatorStyle = UITableViewCell.SeparatorStyle.none
    }

    // MARK: – View Data

    typealias HeaderViewModel = PlaceHeaderView.ViewModel
    private let headerData: HeaderViewModel

    typealias CellViewModel = (title: String, subtitle: String)
    private var cellData: CellViewModel?

    private var numberOfSectionsInTable: Int = 0
    private var numberOfRowsInTable: Int = 0

    private let reuseIdentifier = String.CellIdentifiers.scooter

    // MARK: – UI

    private lazy var header = PlaceHeaderView(withViewModel: self.headerData)
    private weak var viewController: PlaceViewDelegate?
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.separatorStyle = Appearance.tableSeparatorStyle
        return table
    }()

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    init(
        frame: CGRect = .zero,
        withDataForHeader headerData: HeaderViewModel,
        inside viewController: PlaceViewDelegate?
    ) {
        self.headerData = headerData
        self.viewController = viewController
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func updateTotalTextInHeader(byString string: String) {
        self.header.updateTotalText(byString: string)
    }

    final func updateSectionsCount(_ count: Int) {
        self.numberOfSectionsInTable = count
    }

    final func updateRowsCount(_ count: Int) {
        self.numberOfRowsInTable = count
    }

    final func renderCell(withData data: CellViewModel) {
        self.cellData = data
    }

    final func reloadTable() {
        defer {
            self.hideOrShowTableIfNecessary()
        }
        self.viewController?.needsToUpdateTableSections()
        self.viewController?.needsToUpdateTableRows()
        self.tableView.reloadData()
    }

    // MARK: – Private Methods

    private func setupBackground() {
        self.backgroundColor = Appearance.backgroundColor
    }

    private func setupHeader() {
        self.header.backgroundColor = Appearance.backgroundColor
    }

    private func setupTableAndCells() {
        self.tableView.register(
            ScooterTableViewCell.self,
            forCellReuseIdentifier: self.reuseIdentifier
        )
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.hideOrShowTableIfNecessary()
    }

    private func hideOrShowTableIfNecessary() {
        self.tableView.isHidden = self.numberOfRowsInTable <= 0
    }

    private func makeConstraintsForHeader() {
        self.header.translatesAutoresizingMaskIntoConstraints = false
        self.header.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
        }
    }

    private func makeConstraintsForTable() {
        self.tableView.snp.makeConstraints { make in
            make.top.equalTo(self.header.snp.bottom)
            make.left.right.equalToSuperview()
            if #available(iOS 12.0, *) {
                make.bottom.equalTo(self.safeAreaInsets.bottom)
            } else {
                make.bottom.equalToSuperview()
            }
        }
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.header)
        self.addSubview(self.tableView)
    }

    func setupSubviews() {
        self.setupBackground()
        self.setupHeader()
        self.setupTableAndCells()
    }

    func makeConstraints() {
        self.makeConstraintsForHeader()
        self.makeConstraintsForTable()
    }

}

// MARK: – Table View Data Source

extension PlaceView: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.numberOfSectionsInTable
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRowsInTable
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.viewController?.needsToRenderCellAtRow(indexPath.row)
        guard let data = self.cellData else {
            return UITableViewCell()
        }
        let cell = ScooterTableViewCell(
            style: .default,
            reuseIdentifier: self.reuseIdentifier,
            withData: data
        )
        return cell
    }

}

// MARK: – Table View Delegate

extension PlaceView: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Appearance.cellHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewController?.needsToOpenScooterAtRow(indexPath.row)
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let scooterCell = cell as? ScooterTableViewCell {
            scooterCell.clearSpareViews()
            scooterCell.removeFromSuperview()
        }
    }

}
