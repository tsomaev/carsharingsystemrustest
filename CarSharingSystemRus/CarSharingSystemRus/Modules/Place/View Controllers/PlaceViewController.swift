// 
//  PlaceViewController.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

final class PlaceViewController: ResponsiveViewController {

    // MARK: – Appearance

    typealias BarAppearance = NavigationController.Appearance

	// MARK: - Properties

	private var viewModel: PlaceViewModel
    private var alternativeTitle: String = ""

    // MARK: – UI

    private lazy var placeView: PlaceView = {
        let view = PlaceView(
            withDataForHeader: self.viewModel.dataForHeader,
            inside: self
        )
        return view
    }()

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.viewModel = PlaceViewModel()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel.setDelegate(self)
    }

	// MARK: - View Life Cycle

    override func loadView() {
        super.loadView()
        self.view = self.placeView
        self.view.backgroundColor = UIColor.Interface.defaultBackground
    }

	override func viewDidLoad() {
		super.viewDidLoad()
        self.setLoadingDescription(String.LoadingTexts.loadingParkingDetauls)
        self.prepareResponsiveViewWithContentContainer(UIView())
        self.setupNavigationController()
        self.loadScootersIfNecessary()
    }

    // MARK: – Public Methods

    final func saveInModelParkingPlace(_ place: ParkingPlace) {
        self.viewModel.savePlace(place)
    }

    // MARK: – Private Methods

    private func setupNavigationController() {
        self.setupNavigationCloseButton()
        self.setupNavigationTitle()
        self.setupNavigationBarAppearance()
    }

    private func setupNavigationCloseButton() {
        let view = CloseItemView()
        view.set(self)
        let item = UIBarButtonItem(customView: view)
        self.navigationItem.setLeftBarButton(item, animated: false)
    }

    private func setupNavigationTitle() {
        let title = self.viewModel.placeTitle
        if !title.isEmpty {
            self.viewState = .normal
            self.navigationItem.title = title
            self.navigationController?.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.font: BarAppearance.titleFont
            ]
        } else {
            self.viewState = .empty
        }
    }

    private func setupNavigationBarAppearance() {
        if let navigationController = self.navigationController {
            let bar = navigationController.navigationBar
            bar.tintColor = BarAppearance.barTintColor
            bar.isTranslucent = false
            bar.setBackgroundImage(BarAppearance.barBackgroundImage, for: .default)
            bar.shadowImage = BarAppearance.barBackgroundShadowImage
        }
    }

    private func loadScootersIfNecessary() {
        if self.viewModel.rowsCount == 0 {
            self.setLoadingDescription(String.LoadingTexts.loadingScooters)
            self.viewState = .loading
            self.viewModel.loadPossibleScooters()
        } else {
            self.placeView.reloadTable()
        }
    }

}

// MARK: – Navigation Bar Delegate

extension PlaceViewController: NavigationBarDelegate {

    func didTouchCloseButton() {
        self.dismiss(animated: true)
    }

    func didTouchBackButton() {
        fatalError(String.ErrorMessages.unexpectedButton)
    }

}

// MARK: – Place View Delegate

extension PlaceViewController: PlaceViewDelegate {

    func needsToUpdateTableSections() {
        self.placeView.updateSectionsCount(self.viewModel.sectionsCount)
    }

    func needsToUpdateTableRows() {
        self.placeView.updateRowsCount(self.viewModel.rowsCount)
    }

    func needsToRenderCellAtRow(_ row: Int) {
        self.placeView.renderCell(withData: self.viewModel.getCellDataForRow(row))
    }

    func needsToOpenScooterAtRow(_ row: Int) {
        guard let viewControllerToPresent = RentAssembler().makeModule() as? RentViewController else {
            return // other view controller has been initialized..
        }
        guard let scooterToPresent = self.viewModel.scooterToPresent(atRow: row) else {
            return // no scooter at this row!
        }
        viewControllerToPresent.saveInModelScooter(scooterToPresent)
        navigationController?.pushViewController(viewControllerToPresent, animated: true)
    }

}

// MARK: - View Controller with Callbacks

extension PlaceViewController: ViewControllerWithCallbacks {

    func didGetBadErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.setErrorDescription(title: title, message: rawMessage)
        self.viewState = .error
    }

    func didGetNewData() {
        self.placeView.updateTotalTextInHeader(byString: self.viewModel.totalStringInHeader)
        self.placeView.reloadTable()
        self.viewState = .normal
    }

    func didGetEmptyData() {
        self.viewState = .empty
    }
}
