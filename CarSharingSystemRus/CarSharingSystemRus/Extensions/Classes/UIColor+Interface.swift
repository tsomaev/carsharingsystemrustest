//
//  UIColor+Interface.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit.UIColor

extension UIColor {
    enum Interface {
        static let main = UIColor(
            red: 249/255.0,
            green: 56/255.0,
            blue: 34/255.0,
            alpha: 1
        )
        static let defaultBackground = UIColor.white
        static let defaultText = UIColor.black
    }
}
