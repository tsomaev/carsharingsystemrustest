//
//  UIView+Shadows.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit.UIView

extension UIView {

    func applyShadow(
        withColor color: CGColor,
        opacity: Float,
        offset: CGSize,
        radius: CGFloat = 1,
        scale: Bool = true
    ) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

    func clearShadow() {
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.shadowOpacity = 0.0
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 0.0
        self.layer.shadowPath = nil
        self.layer.shouldRasterize = false
        self.layer.rasterizationScale = 0.0
    }

}
