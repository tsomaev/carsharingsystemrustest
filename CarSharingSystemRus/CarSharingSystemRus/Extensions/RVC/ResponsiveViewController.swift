// 
//  ResponsiveViewController.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



/**
 
 `ResponsiveViewController` is a class with interactive behavior to present
 whether it can be error, empty data, sending a network request or just some data (default).
 
 */



class ResponsiveViewController: UIViewController, ErrorViewDelegate {
    

    // MARK: – View State
    
    /**
     
     `ViewState` enumeration is a list to define specific state of view to present.
     ```
     // When view controller gets an error:
     let viewController = ResponsiveViewController()
     print(viewController.view) // prints default state 'loading'.
     viewController.viewState = .error
     ```
     
     */
    
    enum ViewState {
        
        /// Use when data is loading into developing module (default).
        case loading
        
        /// Use when no data to present.
        case empty
        
        /// Use when module has data to present.
        case normal

        /// Use when some error came into developing module.
        case error
        
    }
    
    
    
    
    
    // MARK: – Private Properties
    
    private var _tableView: UITableView?
    private var _viewState: ViewState = .loading
    private var _errorDescription: (title: String, message: String?)?
    private var _loadingDescription:   String?
    private var _contentContainerView: UIView?
    private var _errorContainerView:   ErrorContainerView?
    private var _emptyContainerView:   ErrorContainerView?
    private var _loadingContainerView: LoadingContainerView?
    
    
    
    
    
    // MARK: – Public Properties
    
    final var viewState: ViewState {
        get {
            return self._viewState
        }
        set(newState) {
            defer {
                self.updateView()
            }
            switch newState {
            case .loading:
                self._viewState = .loading
            case .empty:
                self._viewState = .empty
            case .normal:
                self._viewState = .normal
            case .error:
                self._viewState = .error
            }
        }
    }
    
    
    
    
    
    // MARK: – View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        #if DEBUG
        print("\n[\(self) viewDidLoad]\n")
        #endif
    }
    
    
    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }
    
    
    
    
    
    // MARK: – Public Methods
    
    /**
     Use thid method because it is important to setup all necessary containers including your own with future actual data either implemented in xib or right in storyboard.
     
     - Note: Use when it's necessary to render container views programatically having one content container view created in storyboard or xib (loaded before calling this method). After setting these containers view controller will immediately update its view based on `viewState` property value.
     - Precondition: One single parameter `contentContainerView` is an instance of `UIVIew` class. You may create it using Interface Builder or xib but in any case you need to prepare this container view to use its to set up other containers which will be created programatically.
     - Parameter contentContainerView: A container view to present content.
     - Returns: Void.
     
     */
    
    final func prepareResponsiveViewWithContentContainer(_ container: UIView!) {
        // Content Container:
        self._contentContainerView = container
        // Error Container:
        let errorTitle = self._errorDescription?.title ?? "Unknown Error"
        ErrorContainerView.installErrorContainerWithTitle(errorTitle, message: nil, inside: self)
        // Empty Container:
        ErrorContainerView.installEmptyContainerWithTitle(errorTitle, message: nil, inside: self)
        // Loading Container:
        let loadingTitle = self._loadingDescription ?? "Loading.."
        LoadingContainerView.installLoadingContainerWithTitle(loadingTitle, inside: self)
        // Update View state:
        self.hideAllContainers()
        self.updateView()
    }
    
    
    
    
    
    /**
     Use this optional method to fix table (when it exists in view controller) for ability to reload table when it will be necessary or just using the method **reloadTable:**.
     
     - Parameter tableView: a simple UITableView instance.
     - Returns: Void.
     
     */
    
    final func setTableView(_ tableView: UITableView) {
        self._tableView = tableView
    }
    
    
    
    
    
    /**
     Use this method when your view controller needs to present some specific error based on **title** and **message** (optional) error's information.
     
     - Note: use this method before you'll set to **viewState** an **.error** value
     - Parameter title: title error description.
     - Parameter message: optional additional error description. If it's nil then **ErrorView** won't present **UILabel** instance on a screen.
     - Returns: Void.
     
     ```
     // Imagine the moment when view controller has received HTTP 404 from server:
     let viewController = ResponsiveViewController()
     viewController.setErrorDescription(title: "Not found", message: "The resource you've requested is not existing in the path you had defined. 👎🏻")
     viewController.viewState = .error // view controller will show the error with defined description in right above lines.
     ```
     
     */
    
    final func setErrorDescription(title: String, message: String?) {
        let newErrorDescription = (title, message)
        self._errorDescription = newErrorDescription
    }
    
    
    
    
    
    /**
     In addition to title and subtitle labels, `ErrorView` instance has a button whose title and function you can define in specific behavior you need exactly. Default button title value is `Button Title` and that seems boring, yeah. 🙁
     
     - Parameter buttonTitle: a string will used to be in button title text.
     - Precondition: **buttonTitle** parameter cannot be empty.
     - Returns: Void.
     */
    
    final func setErrorButtonTitle(_ buttonTitle: String) {
        self._errorContainerView?.setErrorButtonTitle(buttonTitle)
    }
    
    
    
    
    
    /**
     Use this method when you want to define a text which will be presented on a screen when view controller will request some data to present.
     
     - Note: Without calling this method and setting a **loading** value to **viewState** property, view controller will show loading view with default description `Loading..`.
     - Parameter description: a string will be used in loading view.
     - Returns: Void.
     
     ```
     let viewController = ResponsiveViewController()
     viewController.setLoadingDescription("Waiting for git repository cloning.. 🚀🚀🚀")
     viewController.viewState = .loading // presents loading view controller with specific description above.
     ```
     
     */
    
    final func setLoadingDescription(_ description: String) {
        self._loadingDescription = description
    }
    
    
    
    
    
    /// Just reloads table if table was fixed by calling **setTableView:** method.
    
    final func reloadTable() {
        self._tableView?.reloadData()
    }
    
    
    
    
    
    /// Use this method to register a specific cell's class and with its identifier.
    
    final func register(_ cellClass: AnyClass?, forCellReuseIdentifier cellIdentifier: String) {
        self._tableView?.register(cellClass, forCellReuseIdentifier: cellIdentifier)
    }
    
    
    
    
    
    /// Use this method to fix error container view created programatically:
    /// - Parameter container: Error Container View's instance.
    
    final func setErrorContainerView(_ container: ErrorContainerView) {
        self._errorContainerView = container
    }
    
    
    
    
    
    /// Use this method to fix empty container view created programatically:
    /// - Parameter container: Empty Container View's instance.
    
    final func setEmptyContainerView(_ container: ErrorContainerView) {
        self._emptyContainerView = container
    }
    
    
    
    
    
    /// Use this method to fix loading container view created programatically:
    /// - Parameter container: Loading Container View's instance.
    
    final func setLoadingContainerView(_ container: LoadingContainerView) {
        self._loadingContainerView = container
    }
    
    
    
    
    
    /**
     Override ⚠️ this method to define cells you want register before using them in a table.
     
     - Note: Do not forget to call overrided method in **viewDidLoad:** method for custom cell recognizing by your table!
     
     ```
     // Sample code snippet for child view controller:
     override func registerCells() {
        self.register(YouCustomCellClass1.self, forCellReuseIdentifier: "SomeCellIdentifier1")
        self.register(YouCustomCellClass2.self, forCellReuseIdentifier: "SomeCellIdentifier2")
        self.register(YouCustomCellClassN.self, forCellReuseIdentifier: "SomeCellIdentifierN")
        // etc..
     }
     ```
     
     */
    
    func registerCells() {
        #if DEBUG
        print("\n[\(self) registerCells]\n")
        #endif
    }
    
    
    
    
    
    /**
     Override ⚠️ this method to define behavior when user pulled table down to refresh the data.
     
     - Note: Do not forget to fix table view instance using **setTableView:** method before calling current method!
     
     ```
     // Sample code snippet for child view controller:
     override func didRefreshControlPullDown() {
        self.doSomeDataRequestToRefreshTable()
     }
     ```
     
     */
    
    func didRefreshControlPullDown() {
        #if DEBUG
        print("\n[\(self) didRefreshControlPullDown]\n")
        #endif
    }
    
    
    
    
    
    // MARK: – Private Methods
    
    private func updateView() {
        switch self._viewState {
        case .loading:
            self.presentLoadingView()
        case .empty:
            self.presentEmptyView()
        case .normal:
            self.presentNormalView()
        case .error:
            self.presentErrorView()
        }
    }
    
    
    private func presentLoadingView() {
        if let loadingDescription = self._loadingDescription {
            self._loadingContainerView?.runSpinnerAnimationWithTitle(loadingDescription)
        } else {
            self._loadingContainerView?.runSpinnerAnimation()
        }
        self._loadingContainerView?.isHidden = false
    }
    
    
    private func presentEmptyView() {
        self._contentContainerView?.isHidden = true
        self._errorContainerView?.isHidden = true
        self._loadingContainerView?.isHidden = true
        if let error = self._errorDescription {
            self._emptyContainerView?.setErrorTitle(error.title, withMessage: error.message)
        }
        self._emptyContainerView?.isHidden = false
    }
    
    
    private func presentNormalView() {
        self._errorContainerView?.isHidden = true
        self._loadingContainerView?.isHidden = true
        self._emptyContainerView?.isHidden = true
        self._contentContainerView?.isHidden = false
    }
    
    
    private func presentErrorView() {
        self._loadingContainerView?.isHidden = true
        self._emptyContainerView?.isHidden = true
        self._contentContainerView?.isHidden = true
        if let error = self._errorDescription {
            self._errorContainerView?.setErrorTitle(error.title, withMessage: error.message)
        }
        self._errorContainerView?.isHidden = false
    }
    
    
    private func loadViewsFromXibsToContainers() {
        guard let errorContainer = self._errorContainerView else {
            return
        }
        guard let emptyContainer = self._emptyContainerView else {
            return
        }
        guard let loadingContainer = self._loadingContainerView else {
            return
        }
        self.loadErrorViewFromXibToContainer(errorContainer)
        self.loadEmptyViewFromXibToContainer(emptyContainer)
        self.loadLoadingViewFromXibToContainer(loadingContainer)
    }
    
    
    private func loadErrorViewFromXibToContainer(_ container: ErrorContainerView) {
        let title = self._errorDescription?.title ?? "Unknown Error"
        let message = self._errorDescription?.message ?? ""
        container.prepareErrorSubviewWithTitle(title, message: message, inside: self)
    }
    
    
    private func loadEmptyViewFromXibToContainer(_ container: ErrorContainerView) {
        let title = self._errorDescription?.title ?? "No data"
        let message = self._errorDescription?.message ?? ""
        container.prepareEmptySubviewWithTitle(title, message: message, inside: self)
    }
    
    
    private func loadLoadingViewFromXibToContainer(_ container: LoadingContainerView) {
        if let loadingDescription = self._loadingDescription {
            container.prepareLoadingSubviewWithTitle(loadingDescription, inside: self)
        } else {
            container.prepareLoadingSubviewInside(self)
        }
    }
    
    
    private func hideAllContainers() {
        self._emptyContainerView?.isHidden = true
        self._loadingContainerView?.isHidden = true
        self._contentContainerView?.isHidden = true
        self._errorContainerView?.isHidden = true
    }
    
    
    
    
    
    // MARK: – Error View Delegate
    
    /**
     
     Override ⚠️ this method to define behavior when user has touched the button in error view. For instance, it can be relaunching request failed recently.
     
     ```
     // Sample code snippet for child view controller:
     override func didErrorButtonTouch() {
        self.setLoadingDescription("I am trying to load your data one more time..")
        self.viewState = .loading // presents loading view state.
        self.requestData() // sample method. Here it can be whatever you want to implement.
     }
     ```
     
     */
    
    func didErrorButtonTouch() {
        #if DEBUG
        print("\n[\(self) didErrorButtonTouch]\n")
        #endif
    }

    
}
