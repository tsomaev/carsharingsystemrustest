// 
//  ErrorContainerView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



class ErrorContainerView: UIView {
    
    
    // MARK: – Properties
    
    private var errorSubview: ErrorView!
    
    
    
    
    
    // MARK: – Preparing sub views
    
    final func prepareErrorSubviewWithTitle(_ title: String,
                                              message: String?,
                                              inside viewController: ErrorViewDelegate) {
        
        self.errorSubview = ErrorView.instanceWithTitle(title,
                                                        message: message,
                                                        inside: viewController,
                                                        withType: .error)
        self.addSubViewToItself()
    }
    
    
    final func prepareEmptySubviewWithTitle(_ title: String,
                                            message: String?,
                                            inside viewController: ErrorViewDelegate) {
        
        self.errorSubview = ErrorView.instanceWithTitle(title,
                                                        message: message,
                                                        inside: viewController,
                                                        withType: .empty)
        self.addSubViewToItself()
    }
    
    
    private func addSubViewToItself() {
        if self.errorSubview != nil {
            self.addSubview(self.errorSubview)
            self.errorSubview.frame = self.bounds
            self.errorSubview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
    }
    
    
    
    
    
    // MARK: – Public Methods
    
    final func setErrorTitle(_ title: String, withMessage message: String?) {
        self.errorSubview.setTitle(title, withMessage: message)
    }
    
    
    final func setErrorButtonTitle(_ buttonTitle: String) {
        self.errorSubview.setButtonTitle(buttonTitle)
    }
    
    
    
    
    
    // MARK: – Class Implementations
    
    class func installErrorContainerWithTitle(_ title: String, message: String?, inside viewController: ResponsiveViewController) {
        // 1. Container:
        let container = ErrorContainerView(frame: viewController.view.bounds)
        viewController.view.addSubview(container)
        container.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // 2. Container's sub views:
        container.prepareErrorSubviewWithTitle(title, message: message, inside: viewController)
        viewController.setErrorContainerView(container)
    }
    
    
    class func installEmptyContainerWithTitle(_ title: String, message: String?, inside viewController: ResponsiveViewController) {
        // 1. Container:
        let container = ErrorContainerView(frame: viewController.view.bounds)
        viewController.view.addSubview(container)
        container.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // 2. Container's sub views:
        container.prepareEmptySubviewWithTitle(title, message: message, inside: viewController)
        viewController.setEmptyContainerView(container)
    }
    
    
}