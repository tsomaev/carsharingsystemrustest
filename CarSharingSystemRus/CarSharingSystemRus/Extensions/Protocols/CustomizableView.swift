//
//  CustomizableView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

protocol CustomizableView: class {
    /// Firstly, implement current method to add all necessary subviews in your custom view instance.
    func addSubviews()
    /// Secondly, implement current method to setup each separate subview in your custom view instance.
    func setupSubviews()
    /// And finally, implement current method to setup all necesary constraints for layout in your custom view instace.
    func makeConstraints()
}
