//
//  Assembler.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/22/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

protocol Assembler: class {
    func makeModule() -> UIViewController
}
