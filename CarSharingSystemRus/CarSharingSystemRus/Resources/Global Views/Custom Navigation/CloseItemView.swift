//
//  CloseItemView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/23/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class CloseItemView: UIView, CustomizableView {

    // MARK: – Appearance

    private enum Appearance {
        static let buttonSize = CGSize(
            width: 35, height: 35
        )
        static let buttonMargins: CGFloat = 1
        static let buttonIcon = UIImage(
            named: String.ImageNames.CloseNavigationItem
        )!
    }

    // MARK: - Private Properties

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Appearance.buttonIcon, for: .normal)
        button.widthAnchor
            .constraint(equalToConstant: Appearance.buttonSize.width)
            .isActive = true
        button.heightAnchor
            .constraint(equalToConstant: Appearance.buttonSize.height)
            .isActive = true
        return button
    }()

    private weak var delegate: NavigationBarDelegate?

    // MARK: – Initialization

    required init?(coder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func set(_ delegate: NavigationBarDelegate?) {
        self.delegate = delegate
    }

    // MARK: – Private Methods

    @objc private func action(_ sender: UIButton) {
        self.delegate?.didTouchCloseButton()
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(closeButton)
    }

    func setupSubviews() {
        self.closeButton.addTarget(
            self,
            action: #selector(self.action(_:)),
            for: .touchUpInside
        )
    }

    func makeConstraints() {
        self.closeButton.translatesAutoresizingMaskIntoConstraints = false
        self.closeButton.snp.makeConstraints { make in
            make.edges.equalToSuperview().offset(Appearance.buttonMargins)
        }
    }

}
