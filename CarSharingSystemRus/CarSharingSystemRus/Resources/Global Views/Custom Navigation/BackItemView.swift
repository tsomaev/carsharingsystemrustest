//
//  BackItemView.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class BackItemView: UIView, CustomizableView {

    // MARK: – Apperance

    private enum Appearance {
        static let buttonSize = CGSize(
            width: 35,
            height: 35
        )
        static let buttonMargins: CGFloat = 1
        static let buttonIcon = UIImage(
            named: String.ImageNames.BackNavigationItem
        )!
    }

    // MARK: – UI

    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(Appearance.buttonIcon, for: .normal)
        button.widthAnchor
            .constraint(equalToConstant: Appearance.buttonSize.width)
            .isActive = true
        button.heightAnchor
            .constraint(equalToConstant: Appearance.buttonSize.height)
            .isActive = true
        return button
    }()

    private weak var delegate: NavigationBarDelegate?

    // MARK: – Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError(String.ErrorMessages.noViewDecoderImplementation)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubviews()
        self.setupSubviews()
        self.makeConstraints()
    }

    // MARK: – Public Methods

    final func set(_ delegate: NavigationBarDelegate?) {
        self.delegate = delegate
    }

    // MARK: – Private Methods

    @objc private func action(_ sender: UIButton) {
        self.delegate?.didTouchBackButton()
    }

    // MARK: – CustomizableView

    func addSubviews() {
        self.addSubview(self.backButton)
    }

    func setupSubviews() {
        self.backButton.addTarget(
            self,
            action: #selector(self.action(_:)),
            for: .touchUpInside
        )
    }

    func makeConstraints() {
        self.backButton.translatesAutoresizingMaskIntoConstraints = false
        self.backButton.snp.makeConstraints { make in
            make.edges.equalToSuperview().offset(Appearance.buttonMargins)
        }
    }
}
