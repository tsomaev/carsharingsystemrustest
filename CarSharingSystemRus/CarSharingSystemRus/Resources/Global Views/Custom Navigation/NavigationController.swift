//
//  NavigationController.swift
//  CarSharingSystemRus
//
//  Created by Ivan Shokurov on 7/25/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    public enum Appearance {
        // View:
        static let titleFont = UIFont.custom(ofSize: 20, withWeight: .bold)!
        static let barTintColor = UIColor.Interface.defaultBackground
        static let isBarTranslucent = false
        static let barBackgroundImage = UIImage()
        static let barBackgroundShadowImage = UIImage()
        // Shadow:
        static let shadowColor = UIColor.Interface.defaultText.cgColor
        static let shadowOpacity: Float = 0.2
        static let shadowOffset = CGSize(width: 5, height: 10)
        static let shadowRadius: CGFloat = 20
        static let shadowScale = true
    }

}
